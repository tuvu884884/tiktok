import styles from '../Header/Header.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAd } from '@fortawesome/free-solid-svg-icons'
import { faQuestion } from '@fortawesome/free-solid-svg-icons'

function Support () {
   return (
       <div className={styles.support}>
           <p className={styles.supportItem}>
               <FontAwesomeIcon icon={faAd}  className={styles.icon}/>
               <span className={styles.text}> Tiếng Việt </span>
           </p>
           <p className={styles.supportItem}>
               <FontAwesomeIcon icon={faQuestion}  className={styles.icon}/>
               <span className={styles.text}>Phản hồi và trợ giúp </span>
           </p>
       </div>
   )
}

export default Support
