import { Grid } from '@mycv/mycv-grid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'

import Button from '../../packages/tuvx-button'
import styles from './Header.module.scss'
import logoDark from '../../accssets/img/logo-dark.svg'
import logoTextDark from '../../accssets/img/logo-text-dark.svg'
import SearchBox from '../SearchBox/SearchBox'
import SupportComponent from '../Support'


function Header () {
    return (
        <header className={styles.wrapper}>
          <Grid type='wide'>
              <div className={styles.content}>
                  <div className={styles.logoWrapper}>
                      <img src={logoDark} alt='logo-dark'/>
                      <img src={logoTextDark} alt='logo-text-dark'/>
                  </div>
                <SearchBox/>
                  <div>
                      <Button 
                      type="normal"
                      underline 
                      href='https://www.facebook.com/'
                      >Tải lên</Button>
                      <Button >Đăng Nhập</Button>
                    <div className={styles.ellipsisV}>
                    <FontAwesomeIcon icon={faEllipsisV} className={styles.ellipsisVIcon}/>
                    <SupportComponent />
                    </div>
                  </div>
              </div>
          </Grid>
        </header>
    )
}

export default Header
