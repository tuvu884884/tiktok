import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { faCheck }  from '@fortawesome/free-solid-svg-icons'
import { useState, useCallback } from 'react'

import styles from './SearchBox.module.scss'
import SearchView from '../SearchResultParen/SearchView'

function SearchBox () {
    const [textInput, setTextInput] = useState('')
    function onchangeInput(e) {
        setTextInput(e.target.value)
    }
    function onClicktimesCircle () {
        setTextInput('')
    }
    return (
    <div className={styles.searchBox}>
        <input type='text' placeholder="Tìm kiếm tài khoản" value={textInput} onChange={onchangeInput}/>
        <button className={styles.search}>
            <FontAwesomeIcon icon={faSearch} className={styles.searchIcon}/>
        </button>
        <button className={styles.timesCircle} onClick={onClicktimesCircle}>
            {textInput ? <FontAwesomeIcon icon={faTimesCircle} className={styles.timesCircleIcon}/> : ''}
        </button>
        <SearchView text={textInput} icon={ <FontAwesomeIcon icon={faCheck} className={styles.searchCheck}/>}/>
    </div>
    )
}

export default SearchBox