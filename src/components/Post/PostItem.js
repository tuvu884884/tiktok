import { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart, faComment, faShare ,faLink, faVolumeUp, faVolumeMute} from '@fortawesome/free-solid-svg-icons'
import { faFacebookSquare, faTwitter,faWhatsapp} from '@fortawesome/free-brands-svg-icons'

import styles from './Post.module.scss' 
import Player from 'react-player'
import Button from '../../packages/tuvx-button'
import Share from './Share'
import PostDetail from '../PostDetail'


function PostItem ({
       avatar = '',
       username = '',
       name = '',
       content = '',
       musicName = '',
       icon = '',
       status = '',
       video = '',
       postContent = '',
       isFollow=true,
       likeCount = 0,
       commentCount = 0,
       shareCount = 0,
       onLike = () => {},
       onComment = () => {},
       onShare = () => {},
   
}) {
    const [check, setCheck] = useState(isFollow)
    const [isVolume, setIsVolume] = useState(true)
    function checkFollow() {
         setCheck(!check)
    }
    function ChechIsVolume () {
        setIsVolume(!isVolume)
    }
    const datasIcon = [
        {
        icon: faFacebookSquare,
        text: 'Chia sẻ với Facebook'
        },
        {
        icon: faTwitter,
        text: 'Chia sẻ với Twitter'
        },
        {
        icon: faWhatsapp,
        text: 'Chia sẻ với WhatsApp'
        },
        {
        icon: faLink,
        text: 'Sao chép liên kết'
        },
   ]
    return (
        <div className={styles.postitem}>
            <div className={styles.avatar} >
                <img src={avatar} alt=''/>
            </div>
            <div className={styles.icocContent}>
                <div className={styles.author}>
                    <p className={styles.username}> {username}</p>
                    <p className={styles.name}> {name}</p>
                </div>
                <div className={styles.content}>
                    <p className={styles.title}> {content}</p> 
                    <p className={styles.hags}>
                    </p>
                </div>
                <div className={styles.musicName}>
                    {icon}
                    <span className={styles.name}>{musicName}</span>
                </div>
                <div className={styles.groupVideos}>
                <div className={styles.hangleVideo}>
                <Player
                 url={video}
                 className={styles.videos} 
                 autoPlay 
                 width={video.width} 
                 height={video.height}
                 loop
                 />
                {
                    isVolume ? 
                    <div className={styles.Volume}> <FontAwesomeIcon icon={faVolumeUp} onClick={ChechIsVolume}/></div> : 
                    <div className={styles.Volume}> <FontAwesomeIcon icon={faVolumeMute} onClick={ChechIsVolume}/></div>
                }
                </div>
                 <div className={styles.icon}>
                     <div className={styles.groupIcon}>
                        <div className={styles.groupIconIcon}>
                            <FontAwesomeIcon icon={faHeart}/>
                        </div>
                         <span>{likeCount}</span>
                     </div>
                     <div className={styles.groupIcon}>
                        <div className={styles.groupIconIcon}>
                            <FontAwesomeIcon icon={faComment}/>
                        </div>
                         <span>{commentCount}</span>
                     </div>
                     <div className={styles.groupIcon}>
                        <div className={[styles.groupIconIcon,styles.iconShare].join(' ')}>
                            <FontAwesomeIcon icon={faShare}/>
                               <div className={styles.allShare}>
                               {
                                datasIcon.map((datas, index) => {
                                    return (
                                     <Share
                                        key={index}
                                        icon={datas.icon}
                                        text={datas.text}
                                    />
                                     )
                                })
                               }
                               </div>
                        
                        </div>
                         <span>{shareCount}</span>
                     </div>
                 </div> 
                </div>
                <PostDetail
                    avatar = {avatar}
                    username = {username}
                    name = {name}
                    content = {content}
                    musicName = {musicName}
                    icon = {icon}
                    status = {status}
                    video = {video}
                    postContent = {postContent}
                    isFollow= {true} 
                    likeCount = {likeCount}
                    commentCount = {commentCount}
                    shareCount = {shareCount}
                    onLike 
                    onComment 
                    onShare 
                    check = {check}
                    onclick ={checkFollow}
                />
            </div>

           <div className={styles.following}>
           {check ? 
            <Button type="border" size='s' onclick={checkFollow}>Following</Button> :
            <Button type="normal" size='s' onclick={checkFollow} following='following'> Đang Following</Button> }
           </div>
        </div>
    )
}

export default PostItem
