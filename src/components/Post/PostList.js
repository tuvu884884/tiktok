import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMusic } from '@fortawesome/free-solid-svg-icons'

import styles from './Post.module.scss' 
import PostItem from './PostItem'

function PostList ({
  posts = [],
}) {


    return (
        <div className={styles.postList}>
            {
              posts.map((data, index) => {
                   return (    
                    <PostItem
                        key={index}
                        avatar = {data.user.avatar}
                        username = {data.user.last_name + ' ' + data.user.first_name}
                        name ={ data.user.nickname}
                        content = {data.description}
                        musicName ={data.description}
                        icon = {<FontAwesomeIcon icon={faMusic}  className={styles.iconMusic}/>}
                        video = {data.file_url}
                        likeCount = {data.likes_count}
                        commentCount = {data.comments_count}
                        shareCount = {data.shares_count}
                        onLike
                        onComment
                        onShare
                        />
                   )
                })
            }

        </div>
    )
}

export default PostList
