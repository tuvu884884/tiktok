import SearchResult from '../SearchResult'
import styles from '../../SearchBox/SearchBox.module.scss'

function SearchView ({text, icon}) {
    const datas = [
        {
            id: 1,
            name: 'Hiền Hồ',
            username: 'hienhoofficial',
            tick: true
        },
        {
            id: 2,
            name: 'Phước Đỗ',
            username: 'phuocdoofficial',
            tick: false
        }
    ]
    
   function HideView() {
       if(text) {
           return (
            <div className={styles.searchView}>
            { datas.map( data => 
                <SearchResult 
                key={data.id}
                title={data.name}
                desc={data.username}
                tick={data.tick}
                icon={icon}
                />
            ) }
            <div className={styles.searchResult}>
                <p>Xem Tất Cả Kết Quả Dành Cho "{text}"</p>
            </div>
         </div>
           )
       }
       return null
   }
    return (
    <HideView />
    )
}

export default SearchView

