import styles from '../../SearchBox/SearchBox.module.scss'

function SearchResult ({title, desc, tick = false, icon}) {

    return (
        <div className={styles.searchResult}>
         <p className={styles.title}>{title}</p>
         <span>{tick ? icon : ''}</span>
         <p className={styles.desc}>{desc}</p>
        </div>
    )
}

export default SearchResult
