import { Link } from 'react-router-dom'
import styles from './Button.module.scss'

function Button ({
    to = '',
    href = '',
    type="primary",
    underline = false,
    onNewTab = false,
    size="m",
    children = null,
    onclick = () => {}
}) {
   const className = [
      styles[size],
      styles.wrapper,
      styles[type],
      underline ? styles.underline : ''
   ]
   let Component = 'button'
   let props = {}
   if(href) {
      Component = 'a'
      props.href = href
   }
    return (
     <Component {...props} className={className.join(' ')}>
        <span>{children}</span>
     </Component>
    )
}

export default Button
